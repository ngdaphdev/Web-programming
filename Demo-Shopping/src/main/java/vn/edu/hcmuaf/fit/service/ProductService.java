package vn.edu.hcmuaf.fit.service;

import vn.edu.hcmuaf.fit.model.Product;

import java.util.LinkedList;
import java.util.List;

public class ProductService {
    public static List<Product> getListProduct(){
        List<Product> list = new LinkedList<Product>();
        list.add(new Product(2, "Áo Thun be ", "https://cdn2.yame.vn/pimg/ao-thun-co-tron-on-gian-ngan-ha-space-ver16-0020556/41df38d0-015d-e700-f7d7-0018ac665d36.jpg?w=540&h=756", 255000));
        list.add(new Product(3, "Áo Thun xanh ", "https://cdn2.yame.vn/pimg/ao-thun-co-tron-on-gian-y-nguyen-ban-ver90-0021476/64ab863c-069c-5900-e7dc-00197e32326a.jpg?w=540&h=756", 1200000));
        list.add(new Product(4, "Áo Thun đen", "https://cdn2.yame.vn/pimg/ao-thun-co-tron-on-gian-y-nguyen-ban-ver90-0021471/5ab6e9df-64cd-0f00-50ad-00197e2daa12.jpg?w=540&h=756", 20330));
        list.add(new Product(5, "Áo thun trắng", "https://cdn2.yame.vn/pimg/ao-thun-co-tron-on-gian-than-co-ai-valknut-ver3-0020659/20e84677-cfab-4000-cb68-0018ddf94dfe.jpg?w=540&h=756", 24340000));
        list.add(new Product(6, "Áo sơ mi trắng", "https://cdn2.yame.vn/pimg/so-mi-tay-dai-on-gian-m28-0020642/911b4328-721d-e600-8dc9-0018ef333855.jpg?w=540&h=756", 24000));
        list.add(new Product(7, "Áo sơ mi đen", "https://cdn2.yame.vn/pimg/so-mi-tay-dai-on-gian-m28-0020641/5dbcac7c-f3bb-3a00-5b48-0018ef29638b.jpg?w=540&h=756", 209000));
        list.add(new Product(8, "Áo sơ mi xanh", "https://cdn2.yame.vn/pimg/so-mi-co-lanh-tu-on-gian-y-nguyen-ban-ver28-0021005/68a3cd45-f3f0-3b01-f41e-00198941d453.jpg?w=540&h=756", 26700));
        list.add(new Product(9, "Quần tây đen", "https://cdn2.yame.vn/pimg/quan-tay-y2010-hg11-0019800/51abd442-18f4-3600-7c34-0018b9fbbe0c.jpg?w=540&h=756", 245000));
        list.add(new Product(10, "Áo sơ mi sọc", "https://cdn2.yame.vn/pimg/so-mi-tay-dai-on-gian-y-nguyen-ban-ver33-0021218/e0eb6778-eed3-0300-b1b4-001991e1049f.jpg?w=540&h=756", 250000));
        list.add(new Product(11, "Hoodie đen", "https://cdn2.yame.vn/pimg/ao-khoac-hoodie-zipper-on-gian-y-nguyen-ban-ver64-0021477/8b64e6c1-a028-0100-c102-0019892bbcb5.jpg?w=540&h=756", 500000));
        list.add(new Product(12, "Hoodie xanh", "https://cdn2.yame.vn/pimg/ao-khoac-hoodie-zipper-on-gian-y-nguyen-ban-ver64-0021481/4a5e6cf2-41fa-4e00-ac01-00198930a133.jpg?w=540&h=756", 123456));
        list.add(new Product(13, "Hoodie hồng", "https://cdn2.yame.vn/pimg/ao-khoac-hoodie-zipper-on-gian-y-nguyen-ban-ver64-0021480/79fc3b16-3ba2-3a00-6f64-001989302328.jpg?w=540&h=756", 670000));
        list.add(new Product(14, "Hoodie nâu", "https://cdn2.yame.vn/pimg/ao-khoac-hoodie-zipper-on-gian-y-nguyen-ban-ver64-0021482/85cd9f44-860a-6200-620a-00198931d9fe.jpg?w=540&h=756", 200000));
        list.add(new Product(15, "Quần short đen", "https://cdn2.yame.vn/pimg/quan-short-on-gian-y-nguyen-ban-ver38-0021365/18b489e3-53b3-7b00-e09c-00197e3477bf.jpg?w=540&h=756", 208900));
        list.add(new Product(16, "Quần short xanh", "https://cdn2.yame.vn/pimg/quan-short-on-gian-y-nguyen-ban-ver38-0021370/6db65a96-cc7c-c900-a68e-00197e366bed.jpg?w=540&h=756", 180000));
        list.add(new Product(17, "Quần short hồng", "https://cdn2.yame.vn/pimg/quan-short-on-gian-y-nguyen-ban-ver38-0021368/6b6af28d-b580-ab00-400a-00197e35ad4d.jpg?w=540&h=756", 123900));
        list.add(new Product(18, "Quần short nâu", "https://cdn2.yame.vn/pimg/quan-short-on-gian-y-nguyen-ban-ver38-0021367/a611830a-2253-9b00-197d-00197e3560bf.jpg?w=540&h=756", 67800));
        list.add(new Product(19, "Quần jean xanh", "https://cdn2.yame.vn/pimg/quan-dai-jean-straight-on-gian-y-nguyen-ban-ver5-0020538/df272cc6-6f3b-2e00-23a9-0018ac6b3846.jpg?w=540&h=756", 120000));
        list.add(new Product(20, "Quần jean đen", "https://cdn2.yame.vn/pimg/quan-jean-slimfit-y2010-b42-0019772/82cab467-4480-0c00-e875-001883b1a99d.jpg?w=540&h=756", 200000));
        list.add(new Product(21, "Áo polo đen", "https://cdn2.yame.vn/pimg/ao-thun-co-tru-on-gian-y-nguyen-ban-ver100-0021437/50213663-7832-8000-535e-00198eb53e58.jpg?w=540&h=756", 208000));

        return list;
    }



}
